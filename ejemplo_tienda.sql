﻿-- Creación base de datos de tienda

DROP DATABASE IF EXISTS b20190529;
CREATE DATABASE IF NOT EXISTS b20190529;
USE b20190529;

-- Creación de tablas. Total: 5 (3 tablas, 2 relaciones).

CREATE TABLE clientes(
  dni int AUTO_INCREMENT,
  nombre varchar(50),
  apellidos varchar(100),
  direccion varchar(100),
  fechaNac date,
  PRIMARY KEY (dni)
);

CREATE TABLE productos(
  cod int AUTO_INCREMENT,
  nombre varchar(50),
  precio float (7,2),
  PRIMARY KEY (cod)
);

CREATE TABLE proveedores(
  nif int AUTO_INCREMENT,
  nombre varchar (50),
  direc varchar (100),  
  PRIMARY KEY (nif)
);


CREATE TABLE comprar(
  clientedni int,
  prodcod int,
  PRIMARY KEY (clientedni, prodcod),
  CONSTRAINT fkcomprarclientes FOREIGN KEY (clientedni) REFERENCES clientes (dni),
  CONSTRAINT fkcomprarproductos FOREIGN KEY (prodcod) REFERENCES productos (cod)
);

CREATE TABLE suministrar(
  provnif int,
  prodcod int,
  PRIMARY KEY (provnif, prodcod),
  CONSTRAINT fksuministrarproveedores FOREIGN KEY (provnif) REFERENCES proveedores (nif),
  CONSTRAINT fksuministrarproductos FOREIGN KEY (prodcod) REFERENCES productos (cod)
);

INSERT INTO clientes (nombre, apellidos, direccion, fechaNac) VALUES
  ('Bender',' Doblador Rodriguez','Nueva Nueva York','2996/9/4');

SELECT * FROM clientes;

INSERT INTO productos (nombre, precio) VALUES
  ('Cerveza',2.5);

SELECT * FROM productos;

INSERT INTO proveedores (nombre, direc) VALUES
  ('BenderBrau','Nueva Nueva York');

SELECT * FROM proveedores;